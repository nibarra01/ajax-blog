"use strict";



var ajaxRequest1 = $.ajax("data/my-ajax-posts.json");
ajaxRequest1.done(function (data){
    console.log("data:");
    console.log(data);
    var dataHTML = buildHTML(data);
    $('#my-posts').html(dataHTML);

});

function buildHTML(cell){
    console.log("cell:");
    console.log(cell);
    var cellHTML = '';
    cell.forEach(function (post){
        console.log("post:");
        console.log(post);
        console.log(typeof post);
        console.log('---');
        console.log(typeof post.title);
        console.log(typeof post.content);
        console.log(typeof post.categories);
        console.log(post.categories);
        console.log(typeof post.date);
        console.log("---");
        cellHTML += "<div class='row blogpost p-2 m-2'>";
            cellHTML += "<div class='col-12 blogpost-title-div text-center'>";
                cellHTML += "<span class='blogpost-title-text'>";
                    cellHTML += post.title;
                cellHTML += "</span>";
            cellHTML += "</div>";
            cellHTML += "<div class='col-12 blogpost-content-div'>";
                cellHTML += "<span class='blogpost-content-text'>";
                    cellHTML += post.content;
                cellHTML += "</span>";
            cellHTML += "</div>";
            cellHTML += "<div class='col-12 blogpost-categories-div'>";
                cellHTML += "<span class='blogpost-categories-text'>";
                    cellHTML += post.categories;
                    // some code to add in spaces/format/etc
                cellHTML += "</span>";
            cellHTML += "</div>";
            cellHTML += "<div class='col-12 blogpost-date-div text-right'>";
                cellHTML += "<span class='blogpost-date-text'>";
                    cellHTML += post.date;
                    // some code here to convert number into 'Month day, year'
                cellHTML += "</span>";
            cellHTML += "</div>";
        cellHTML += "</div>";

    });
    return cellHTML;
}